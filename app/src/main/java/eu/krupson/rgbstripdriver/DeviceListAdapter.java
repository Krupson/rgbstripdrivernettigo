package eu.krupson.rgbstripdriver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class DeviceListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<DeviceItem> objects;

    private class ViewHolder {
        TextView name;
        TextView address;
    }

    public DeviceListAdapter(Context context, ArrayList<DeviceItem> objects) {
        inflater = LayoutInflater.from(context);
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public DeviceItem getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.device_single, null);
            holder.name = (TextView) convertView.findViewById(R.id.textDeviceName);
            holder.address = (TextView) convertView.findViewById(R.id.textDeviceAddress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(objects.get(position).getName());
        holder.address.setText(objects.get(position).getAddress());
        return convertView;
    }
}
