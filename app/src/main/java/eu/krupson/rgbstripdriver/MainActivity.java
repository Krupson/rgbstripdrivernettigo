package eu.krupson.rgbstripdriver;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.SurfaceView;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements ConnectionStatusListener {

    private BluetoothConnection btConnection;
    private TextView connectionStatus;
    private SeekBar seekHue;
    private SeekBar seekSat;
    private SeekBar seekVal;
    private SurfaceView colorPreview;
    private boolean instantApply = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btConnection = BluetoothConnection.getInstance();
        btConnection.setConnectionStatusListener(this);

        connectionStatus = (TextView) findViewById(R.id.label_connection_status);
        seekHue = (SeekBar) findViewById(R.id.seek_hue);
        seekSat = (SeekBar) findViewById(R.id.seek_saturation);
        seekVal = (SeekBar) findViewById(R.id.seek_value);
        colorPreview = (SurfaceView) findViewById(R.id.color_preview);

        OnSeekBarChangeListener seekListener = new OnSeekBarChangeListener();
        seekHue.setOnSeekBarChangeListener(seekListener);
        seekSat.setOnSeekBarChangeListener(seekListener);
        seekVal.setOnSeekBarChangeListener(seekListener);

        float initialColor[] = {0,0,1};
        colorPreview.setBackgroundColor(Color.HSVToColor(0xFF, initialColor));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btConnection.isConnected()) {
                    try {
                        //btConnection.sendData("V=0;GO;");
                        btConnection.disconnect();
                    } catch(IOException e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Intent intent = new Intent(MainActivity.this, SelectDeviceActivity.class);
                    startActivityForResult(intent, 1);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            String bluetoothName = data.getStringExtra("BT_NAME");
            String bluetoothAddress = data.getStringExtra("BT_ADDR");

            btConnection.connect(bluetoothName, bluetoothAddress);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_instant_apply) {
            instantApply = !instantApply;
            item.setChecked(instantApply);
        } else if(id == R.id.action_animation_start) {
            try {
                btConnection.sendData("GO;");
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setState(int resourceId) {
        connectionStatus.setText(getString(resourceId));
    }

    @Override
    public void setConnectedWith(String name) {
        String state = String.format(getString(R.string.bluetooth_state_connected_with), name);
        connectionStatus.setText(state);
        try{
            btConnection.sendData("H="+seekHue.getProgress()+";S="+seekSat.getProgress()+";V="+seekVal.getProgress()+";GO");
        } catch(IOException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setConnectingWith(String name) {
        String state = String.format(getString(R.string.bluetooth_state_connecting_with), name);
        connectionStatus.setText(state);
    }

    private class OnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            float hsv[] = {
                    seekHue.getProgress(),
                    (float)(seekSat.getProgress() / 100.0),
                    (float)(seekVal.getProgress() / 100.0)
            };

            colorPreview.setBackgroundColor(Color.HSVToColor(0xFF, hsv));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            StringBuilder data = new StringBuilder();
            switch (seekBar.getId()) {
                case R.id.seek_hue:
                    data.append("H=");
                    break;
                case R.id.seek_saturation:
                    data.append("S=");
                    break;
                case R.id.seek_value:
                    data.append("V=");
                    break;

                default: return;
            }

            data.append(seekBar.getProgress());
            data.append(";");

            if(instantApply) {
                data.append("GO;");
            }

            try {
                btConnection.sendData(data.toString());
            } catch(IOException e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
