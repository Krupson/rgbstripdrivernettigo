package eu.krupson.rgbstripdriver;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Set;

public class SelectDeviceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_device);

        ListView devices = (ListView) findViewById(R.id.devices_list);
        Set<BluetoothDevice> pairedDevices = BluetoothConnection.getInstance().getDevicesList();

        ArrayList<DeviceItem> objects = new ArrayList<>();
        DeviceListAdapter deviceAdapter = new DeviceListAdapter(this, objects);

        for(BluetoothDevice device:pairedDevices) {
            objects.add(new DeviceItem(device.getName(), device.getAddress()));
        }

        assert devices != null;
        devices.setAdapter(deviceAdapter);

        devices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView devices = (ListView) findViewById(R.id.devices_list);
                assert devices != null;
                DeviceItem item = (DeviceItem) devices.getItemAtPosition(position);

                setResult(Activity.RESULT_OK, new Intent().putExtra("BT_NAME", item.getName()).putExtra("BT_ADDR", item.getAddress()));
                finish();
            }
        });
    }
}
