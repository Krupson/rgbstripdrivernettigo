package eu.krupson.rgbstripdriver;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public final class BluetoothConnection {

    private static final BluetoothConnection instance = new BluetoothConnection();

    private static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private String name = null;
    private String addr = null;
    private BluetoothSocket btSocket = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private ConnectionStatusListener connectionStatusListener = null;

    private BluetoothConnection() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public static BluetoothConnection getInstance() {
        return instance;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setConnectionStatusListener(ConnectionStatusListener connectionStatusListener) {
        this.connectionStatusListener = connectionStatusListener;
    }

    public void connect(String name, String addr) {
        setAddr(addr);
        setName(name);

        if(connectionStatusListener != null) {
            connectionStatusListener.setConnectingWith(name);
        }
        new BluetoothConnectionAsync().execute();
    }

    public boolean isConnected() {
        return btSocket != null && btSocket.isConnected();
    }

    public void disconnect() throws IOException {
        if(btSocket != null && btSocket.isConnected()) {
            btSocket.close();
            if(connectionStatusListener != null) {
                connectionStatusListener.setState(R.string.bluetooth_state_no_connection);
            }
        }
    }

    public Set<BluetoothDevice> getDevicesList() {
        return mBluetoothAdapter.getBondedDevices();
    }

    public void sendData(String data) throws IOException {
        if(btSocket != null && btSocket.isConnected()) {
            btSocket.getOutputStream().write(data.getBytes());
        }
    }

    private class BluetoothConnectionAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(addr);
                btSocket = device.createInsecureRfcommSocketToServiceRecord(myUUID);
                btSocket.connect();
            } catch(Exception e) {
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(btSocket != null && btSocket.isConnected()) {
                if(connectionStatusListener != null) {
                    connectionStatusListener.setConnectedWith(name);
                }
            } else {
                if(connectionStatusListener != null) {
                    connectionStatusListener.setState(R.string.bluetooth_state_no_connection);
                }
            }
        }
    }

}
