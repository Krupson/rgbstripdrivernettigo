package eu.krupson.rgbstripdriver;

interface ConnectionStatusListener {
    void setState(int resourceId);
    void setConnectedWith(String name);
    void setConnectingWith(String name);
}